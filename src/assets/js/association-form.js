export {listenActions};

function listenActions() {
    jQuery(document).ready(function(){

        jQuery('#associationFormModal .modal-closer').on('click', function(){
            jQuery('#associationFormModal').removeClass('is-active');
        });

        jQuery('#associationFormModal button.delete').on('click', function(){
            jQuery(this).parent().removeClass('is-active');
            return false;
        });
    });
}
