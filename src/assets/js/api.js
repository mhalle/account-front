import axios from 'axios';
const BASE_URL = 'http://account-back';

export let BackEndAPI = {
    editCategory: function (id, name) {
        const url = `${BASE_URL}/api/bank-account-category/edit`;
        return axios.post(url, {
            id: id,
            name: name
        }).then(response => response.data);
    },
    addCategory: function (name) {
        const url = `${BASE_URL}/api/bank-account-category/add`;
        return axios.post(url, {
            name: name
        }).then(response => response.data);
    },
    getCategories: function () {
        const url = `${BASE_URL}/api/bank-account-category/get/all/`;
        return axios.get(url).then(response => response.data);
    },
    removeCategory: function (id) {
        const url = `${BASE_URL}/api/bank-account-category/delete/` + id;
        return axios.get(url).then(response => {
            return response.data;
        })
            .catch(error => {
                return error.response.data;
            });
    },
    editAssociation: function (id, name, category) {
        const url = `${BASE_URL}/api/category-matcher/edit`;
        return axios.post(url, {
            id: id,
            name: name,
            category: category
        }).then(response => response.data);
    },
    addAssociation: function (name, category) {
        const url = `${BASE_URL}/api/category-matcher/add`;
        return axios.post(url, {
            name: name,
            category: category
        }).then(response => response.data);
    },
    getAssociations: function () {
        const url = `${BASE_URL}/api/category-matcher/get/all/`;
        return axios.get(url).then(response => response.data);
    },
    removeAssociation: function (id) {
        const url = `${BASE_URL}/api/category-matcher/delete/` + id;
        return axios.get(url).then(response => {
            return response.data;
        })
            .catch(error => {
                return error.response.data;
            });
    },
    editRedundant: function (id, name, category, credit, debit) {
        const url = `${BASE_URL}/api/bank-account-redundant/edit`;
        return axios.post(url, {
            id: id,
            name: name,
            category: category,
            credit: credit,
            debit: debit
        }).then(response => response.data);
    },
    addRedundant: function (name, category, credit, debit) {
        const url = `${BASE_URL}/api/bank-account-redundant/add`;
        return axios.post(url, {
            name: name,
            category: category,
            credit: credit,
            debit: debit
        }).then(response => response.data);
    },
    getRedundants: function () {
        const url = `${BASE_URL}/api/bank-account-redundant/get/all/`;
        return axios.get(url).then(response => response.data);
    },
    removeRedundant: function (id) {
        const url = `${BASE_URL}/api/bank-account-redundant/delete/` + id;
        return axios.get(url).then(response => {
            return response.data;
        })
        .catch(error => {
            return error.response.data;
        });
    },
    importBankAccount: function(file) {
        let FormData = require('form-data');
        const url = `${BASE_URL}/api/bank-account/import`;
        let importForm = new FormData();
        importForm.append('file', file);

        return axios.post(url, importForm).then(response => response.data);
    }
}