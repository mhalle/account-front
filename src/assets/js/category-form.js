export {listenActions};

function listenActions() {
    jQuery(document).ready(function(){

        jQuery('#categoryFormModal .modal-closer').on('click', function(){
            jQuery('#categoryFormModal').removeClass('is-active');
        });

        jQuery('#categoryFormModal button.delete').on('click', function(){
            jQuery(this).parent().removeClass('is-active');
            return false;
        });
    });
}
