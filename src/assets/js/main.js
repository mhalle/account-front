import Vue from 'vue'
import router from './router'
import App from './../../App'

let vm = new Vue({
    el: document.getElementById('app'),
    router,
    components: {App},
    render (h) {
        return h('App')
    }
});

