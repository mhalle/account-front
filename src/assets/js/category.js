export {removeConfirmation, displayCategoryForm};

function removeConfirmation() {
    jQuery('#deleteConfirmation').addClass('is-active');

    jQuery('.modal-closer').on('click', function(){
        jQuery('#deleteConfirmation').removeClass('is-active');
    });
}

function displayCategoryForm() {
    jQuery('#categoryFormModal').addClass('is-active');

    jQuery('.modal-closer').on('click', function(){
        jQuery('#categoryFormModal').removeClass('is-active');
    });
}
