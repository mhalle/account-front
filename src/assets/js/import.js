export {listenFileUpdate};

function listenFileUpdate() {
    var file = document.getElementById("import");
    file.onchange = function () {
        if (file.files.length > 0) {

            document.getElementById('import_filename').innerHTML = file.files[0].name;

        }
    };
}