export {listenActions};

function listenActions() {
    jQuery(document).ready(function(){
        jQuery('.notification > button.delete').on('click', function(){
            jQuery(this).parent().addClass('is-hidden');
            return false;
        });

        jQuery('#deleteConfirmation button.delete').on('click', function(){
            jQuery(this).parent().removeClass('is-active');
            return false;
        });
    });
}