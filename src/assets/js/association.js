export {removeConfirmation, displayAssociationForm};

function removeConfirmation() {
    jQuery('#deleteConfirmation').addClass('is-active');

    jQuery('.modal-closer').on('click', function(){
        jQuery('#deleteConfirmation').removeClass('is-active');
    });
}

function displayAssociationForm() {
    jQuery('#associationFormModal').addClass('is-active');

    jQuery('.modal-closer').on('click', function(){
        jQuery('#associationFormModal').removeClass('is-active');
    });
}
