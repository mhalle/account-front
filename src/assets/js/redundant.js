export {removeConfirmation, displayRedundantForm};

function removeConfirmation() {
    jQuery('#deleteConfirmation').addClass('is-active');

    jQuery('.modal-closer').on('click', function(){
        jQuery('#deleteConfirmation').removeClass('is-active');
    });
}

function displayRedundantForm() {
    jQuery('#redundantFormModal').addClass('is-active');

    jQuery('.modal-closer').on('click', function(){
        jQuery('#redundantFormModal').removeClass('is-active');
    });
}
