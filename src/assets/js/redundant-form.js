export {listenActions};

function listenActions() {
    jQuery(document).ready(function(){

        jQuery('#redundantFormModal .modal-closer').on('click', function(){
            jQuery('#redundantFormModal').removeClass('is-active');
        });

        jQuery('#redundantFormModal button.delete').on('click', function(){
            jQuery(this).parent().removeClass('is-active');
            return false;
        });
    });
}
