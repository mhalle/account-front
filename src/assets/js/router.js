import Vue from 'vue'
import Router from 'vue-router'
const Home  = () => import('../../views/Home');
const Stats = () => import('../../views/Stats');
const Categories = () => import('../../views/Categories');
const Associations = () => import('../../views/Associations');
const Redundants = () => import('../../views/Redundants');
const Import = () => import('../../views/Import');

Vue.use(Router)

export default new Router({
    routes: [
        { path: '/', name: 'Home', component: Home},
        { path: '/stats', name: 'Stats', component: Stats},
        { path: '/categories', name: 'Categories', component: Categories},
        { path: '/associations', name: 'Associations', component: Associations},
        { path: '/redundants', name: 'Redundants', component: Redundants},
        { path: '/import', name: 'Import', component: Import},
    ]
})