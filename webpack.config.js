const path = require('path');
const UglifyJsPlugin =  require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const CleanWebpackPlugin = require('clean-webpack-plugin')
const webpack = require('webpack');

const dev = process.env.NODE_ENV === "dev"

let cssLoaders = [
    {loader: 'css-loader', options: { importLoaders: 1}}
]

let config = {
    entry: {
        main: ['./src/assets/sass/main.scss','./src/assets/js/main.js']
    },
    node: {
        fs: "empty"
    },
    watch: dev,
    devtool: dev ? "cheap-module-eval-source-map" : false,
    output: {
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/dist/',
        filename: 'main.js'
    },
    resolve: {
        extensions: ['.js', '.css', '.scss', '.vue']
    },
    devServer: {
        noInfo: true
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: ['babel-loader']
            },
            {
                test: /\.vue$/,
                use: 'vue-loader'
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                  fallback: "style-loader",
                  use: cssLoaders
                })
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                  fallback: "style-loader",
                  use: [...cssLoaders, 'sass-loader']
                })
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8192,
                            name: '[name].[hash:7].[ext]'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new ExtractTextPlugin({
            filename: '[name].css',
            disable: dev
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jquery: 'jquery',
            'window.jQuery': 'jquery',
            jQuery: 'jquery'
        })
    ]
}

if (!dev) {
    config.plugins.push(
        new UglifyJsPlugin({
            sourceMap: true
        })
    )
}

module.exports = config